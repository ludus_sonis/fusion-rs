/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/fusion.
 *
 * Fusion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fusion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Fusion.  If not, see <https://www.gnu.org/licenses/>.
 */

//! Configuration of sensors is made through a toml file

use crate::types::Float;
use serde::Deserialize;
use std::fs;
use std::io::Result;

#[derive(Deserialize)]
pub(crate) struct SensorConfig {
    pub(crate) biases: [Float; 3],
    pub(crate) scales: Option<[Float; 3]>,
    pub(crate) units: [Float; 3],
    pub(crate) vars: [Float; 3],
}

#[derive(Deserialize)]
pub(crate) struct AccelConfig(pub SensorConfig);
#[derive(Deserialize)]
pub(crate) struct GyroConfig(pub SensorConfig);

#[derive(Deserialize)]
pub(crate) struct MagnConfig(pub SensorConfig);

#[derive(Deserialize)]
pub(crate) struct Config {
    pub(crate) accel: AccelConfig,
    pub(crate) gyro: GyroConfig,
    pub(crate) magn: MagnConfig,
}

impl Config {
    pub(crate) fn read(file: &str) -> Result<Config> {
        let config_str = fs::read_to_string(file)?;
        let config: Config = toml::from_str(&config_str)?;
        Ok(config)
    }
}
