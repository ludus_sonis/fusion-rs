/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/fusion.
 *
 * Fusion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fusion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Fusion.  If not, see <https://www.gnu.org/licenses/>.
 */
use crate::types::{Float, MatrixF, VectorF};
use na::allocator::Allocator;
use na::{one, DefaultAllocator, DimName};
use nalgebra as na;

/// Kalman process
#[derive(Default)]
pub(super) struct KProcess<V> {
    ///state
    pub(super) x: V,
    ///covariance
    pub(super) p: V,
    ///gain
    pub(super) k: V,
}

impl<D> KProcess<VectorF<D>>
where
    D: DimName,
    DefaultAllocator: Allocator<Float, D>,
{
    /// Equation is x = Ax + Bu, A is identity => x = x + Bu
    pub(super) fn predict_state(&mut self, b: &MatrixF<D>, u: &VectorF<D>)
    where
        DefaultAllocator: Allocator<Float, D, D>,
    {
        self.x.gemv(one(), b, u, one());
    }
    /// Equation is P = APtA + Q, A is identity, Q is diagonal then P + Q is
    /// and we only store diagonal, we treat P and Q as vectors
    pub(super) fn predict_error(&mut self, q: &VectorF<D>) {
        self.p.axpy(one(), q, one());
    }
    /// Equation is K = PtH(HPtH + R)^-1, H is identity so
    /// P(P+R)^-1 and P+R diagonal each coef > 0, we only store diagonal
    pub(super) fn compute_gain(&mut self, r: &VectorF<D>) {
        self.k = self.p.zip_map(r, |p, r| (1.0 / (p + r)) * p);
    }
    /// Equation is x = x + K(z - Hx), H is identity so
    /// x + K(z - x), K diagonal
    pub(super) fn correct_state(&mut self, z: &VectorF<D>) {
        self.x
            .zip_zip_apply(&self.k, z, |x, k, z| x + (k * (z - x)));
    }
    /// Equation is P = (I - KH)P, again H identity so (I - K)P,
    /// K and P diag
    pub(super) fn correct_error(&mut self) {
        self.p.zip_apply(&self.k, |p, k| p - (k * p));
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::types::{MatrixF2, VectorF2};

    #[test]
    fn test_predict_state() {
        let mut kp = KProcess::<VectorF2>::default();
        kp.x = VectorF2::from_element(1.0);
        let b = MatrixF2::new(1.0, 2.0, -1.0, -2.0);
        let u = VectorF2::new(1.0, -1.0);
        kp.predict_state(&b, &u);
        assert_eq!(kp.x, VectorF2::new(0.0, 2.0));
    }

    #[test]
    fn test_predict_error() {
        let mut kp = KProcess::<VectorF2>::default();
        kp.p = VectorF2::from_element(1.0);
        let q = VectorF2::new(1.0, 2.0);
        kp.predict_error(&q);
        assert_eq!(kp.p, VectorF2::new(2.0, 3.0));
    }

    #[test]
    fn test_compute_gain() {
        let mut kp = KProcess::<VectorF2>::default();
        kp.p = VectorF2::new(1.0, 2.0);
        let r = VectorF2::new(3.0, 2.0);
        kp.compute_gain(&r);
        assert_eq!(kp.k, VectorF2::new(0.25, 0.5));
    }

    #[test]
    fn test_correct_state() {
        let mut kp = KProcess::<VectorF2>::default();
        kp.x = VectorF2::from_element(1.0);
        kp.k = VectorF2::new(1.0, 2.0);
        let z = VectorF2::new(2.0, 3.0);
        kp.correct_state(&z);
        assert_eq!(kp.x, VectorF2::new(2.0, 5.0));
    }

    #[test]
    fn test_correct_error() {
        let mut kp = KProcess::<VectorF2>::default();
        kp.p = VectorF2::new(1.0, 2.0);
        kp.k = VectorF2::new(0.5, 0.5);
        kp.correct_error();
        assert_eq!(kp.p, VectorF2::new(0.5, 1.0));
    }
}
