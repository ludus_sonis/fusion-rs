/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/fusion.
 *
 * Fusion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fusion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Fusion.  If not, see <https://www.gnu.org/licenses/>.
 */
#[cfg(target_pointer_width = "32")]
pub(crate) type Float = f32;
#[cfg(target_pointer_width = "64")]
pub(crate) type Float = f64;

use nalgebra as na;
pub(crate) type VectorF<D> = na::VectorN<Float, D>;
pub(crate) type VectorF2 = VectorF<na::U2>;
pub(crate) type VectorF3 = VectorF<na::U3>;
pub(crate) type MatrixF<D> = na::MatrixN<Float, D>;
pub(crate) type MatrixF2 = MatrixF<na::U2>;
pub(crate) type MatrixF3 = MatrixF<na::U3>;

pub(crate) type Matrixf3 = na::MatrixN<f32, na::U3>;
