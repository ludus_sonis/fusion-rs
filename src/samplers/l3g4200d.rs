/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/fusion.
 *
 * Fusion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fusion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Fusion.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::fs::File;
use std::io::{Read, Result};
use std::time::Duration;

use crate::{
    devsampler::DevSamplerTrait,
    sample::Sample,
    types::{Float, VectorF3},
};
use zerocopy::{AsBytes, FromBytes};

#[repr(C, packed)]
#[derive(AsBytes, Default, FromBytes)]
struct L3g4200dData {
    g: [i16; 3],
    _pad: i16,
    t: i64,
}

pub struct L3g4200d {
    rawdata: L3g4200dData,
    consumed: bool,
}

impl L3g4200d {
    pub fn new() -> Self {
        let rawdata = L3g4200dData::default();
        let consumed = false;
        Self { rawdata, consumed }
    }
}

impl DevSamplerTrait for L3g4200d {
    fn get_sample(&mut self, rdr: &mut File) -> Result<Option<Sample>> {
        if self.consumed {
            self.consumed = false;
            return Ok(None);
        }
        self.consumed = true;
        rdr.read_exact(self.rawdata.as_bytes_mut())?;
        let g = self.rawdata.g;
        let v = VectorF3::from_iterator(g.iter().map(|e| i16::from_le(*e) as Float));
        let t = Duration::from_nanos(u64::from_le(self.rawdata.t as u64));
        Ok(Some(Sample::Gyro(v, t)))
    }
}
