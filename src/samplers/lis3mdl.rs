/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/fusion.
 *
 * Fusion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fusion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Fusion.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::fs::File;
use std::io::{Read, Result};

use crate::{
    devsampler::DevSamplerTrait,
    sample::Sample,
    types::{Float, VectorF3},
};
use zerocopy::{AsBytes, FromBytes};

#[repr(C, packed)]
#[derive(AsBytes, Default, FromBytes)]
struct Lis3mdlData([i16; 3]);

pub struct Lis3mdl {
    rawdata: Lis3mdlData,
    consumed: bool,
}

impl Lis3mdl {
    pub fn new() -> Self {
        let rawdata = Lis3mdlData::default();
        let consumed = false;
        Self { rawdata, consumed }
    }
}

impl DevSamplerTrait for Lis3mdl {
    fn get_sample(&mut self, rdr: &mut File) -> Result<Option<Sample>> {
        if self.consumed {
            self.consumed = false;
            return Ok(None);
        }
        self.consumed = true;
        rdr.read_exact(self.rawdata.as_bytes_mut())?;
        let a = self.rawdata.0; // https://github.com/rust-lang/rust/issues/46043
        let v = VectorF3::from_iterator(a.iter().map(|e| i16::from_le(*e) as Float));
        Ok(Some(Sample::Magn(v)))
    }
}
