/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/fusion.
 *
 * Fusion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fusion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Fusion.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::collections::VecDeque;
use std::io::Result;
use std::os::unix::io::{AsRawFd, RawFd};
use std::vec::Vec;

use crate::{calibrator::Calibrator, config::Config, devsampler::DevSampler, sample::Sample};

/// IIO devices set that gather and calibrate samples for each
/// sensors
pub(crate) struct Sampler {
    epollfd: RawFd,
    v: Vec<DevSampler>,
    q: VecDeque<Sample>,
    cal: Calibrator,
}

use epoll::{self, ControlOptions::EPOLL_CTL_ADD, Event, Events};

const MAX_EVENTS: usize = 4; // if one device per sensor
impl Sampler {
    pub(crate) fn new(v: Vec<DevSampler>, c: &Config) -> Result<Self> {
        let epollfd = epoll::create(true)?;
        for (i, ds) in v.iter().enumerate() {
            epoll::ctl(
                epollfd,
                EPOLL_CTL_ADD,
                ds.rdr.as_raw_fd(),
                Event::new(Events::EPOLLIN, i as u64),
            )?;
        }
        let q = VecDeque::<Sample>::with_capacity(MAX_EVENTS);
        let cal = Calibrator::new(c);
        Ok(Self { epollfd, v, q, cal })
    }
}

impl Iterator for Sampler {
    type Item = Result<Sample>;
    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if !self.q.is_empty() {
                return Some(Ok(self.q.pop_front().unwrap()));
            }
            let mut evlist = [Event { events: 0, data: 0 }; MAX_EVENTS];
            let n = match epoll::wait(self.epollfd, -1, &mut evlist) {
                Ok(n) => n,
                Err(e) => return Some(Err(e)),
            };
            for i in 0..n {
                if (evlist[i].events | &Events::EPOLLIN.bits()) == 0 {
                    continue;
                }
                let index = evlist[i].data as usize;
                let ds = &mut self.v[index];
                let (dst, rdr) = (&mut ds.dst, &mut ds.rdr);
                loop {
                    match dst.get_sample(rdr) {
                        Ok(Some(sample)) => self.q.push_back(self.cal.calibrate(sample)),
                        Ok(None) => break,
                        Err(e) => return Some(Err(e)),
                    }
                }
            }
        }
    }
}
