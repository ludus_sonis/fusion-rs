/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/fusion.
 *
 * Fusion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fusion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Fusion.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::config::{Config, SensorConfig};
use crate::sample::Sample;
use crate::types::VectorF3;

/// Calibrates a 3 axes device (accelerometer, gyroscope, magnetometer)
struct Calibrator3DOF {
    biases: VectorF3,
    units: VectorF3,
    scales: Option<VectorF3>,
}

impl Calibrator3DOF {
    fn new(c: &SensorConfig) -> Self {
        let (biases, units, scales) = (
            VectorF3::from(c.biases),
            VectorF3::from(c.units),
            c.scales.map(VectorF3::from),
        );
        Self {
            biases,
            units,
            scales,
        }
    }
    fn calibrate(&self, v: VectorF3) -> VectorF3 {
        let v = v.component_mul(&self.units) - self.biases;
        if let Some(scales) = self.scales {
            return v.component_mul(&scales);
        }
        v
    }
}

/// Calibrator for the sensor set
pub(crate) struct Calibrator {
    accel: Calibrator3DOF,
    gyro: Calibrator3DOF,
    magn: Calibrator3DOF,
}

impl Calibrator {
    pub(crate) fn new(c: &Config) -> Self {
        let accel = Calibrator3DOF::new(&c.accel.0);
        let gyro = Calibrator3DOF::new(&c.gyro.0);
        let magn = Calibrator3DOF::new(&c.magn.0);
        Self { accel, gyro, magn }
    }

    pub(crate) fn calibrate(&self, sample: Sample) -> Sample {
        match sample {
            Sample::Accel(v) => Sample::Accel(self.accel.calibrate(v)),
            Sample::Gyro(v, t) => Sample::Gyro(self.gyro.calibrate(v), t),
            Sample::Magn(v) => Sample::Magn(self.magn.calibrate(v)),
        }
    }
}
