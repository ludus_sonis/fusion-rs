/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/fusion.
 *
 * Fusion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fusion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Fusion.  If not, see <https://www.gnu.org/licenses/>.
 */

mod kalman;

use crate::{
    config::Config,
    devsampler::DevSampler,
    sample::Sample,
    sampler::Sampler,
    types::{Float, MatrixF2, MatrixF3, Matrixf3, VectorF2, VectorF3},
};

use kalman::KProcess;

use std::{
    io::{stdout, Error, ErrorKind, Result, Write},
    mem::size_of,
    slice::from_raw_parts,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
    time::Duration,
    vec::Vec,
};

use clap::{crate_authors, crate_description, crate_name, crate_version, App, Arg};

#[cfg(target_pointer_width = "32")]
fn as_secs_f(d: Duration) -> Float {
    d.as_secs_f32()
}
#[cfg(target_pointer_width = "64")]
fn as_secs_f(d: Duration) -> Float {
    d.as_secs_f64()
}

pub struct Fusion {
    sampler: Sampler,
    /// Direct Cosine Matrix
    dcm: MatrixF3,
    /// Kalman process gyroscope-accelerometer
    kga: KProcess<VectorF3>,
    /// Kalman process gyroscope-magnetometer
    kgm: KProcess<VectorF2>,
    /// Sampling variances
    va: VectorF3,
    vg: VectorF3,
    vm: VectorF3,
}

impl Fusion {
    pub fn new(devsamplers: Vec<DevSampler>, config_file: &str) -> Result<Self> {
        let config = Config::read(config_file)?;
        let sampler = Sampler::new(devsamplers, &config)?;
        let dcm = Self::dcm_from_roll_pitch_yaw(2, 0.0, 0.0, 0.0);
        let kga: KProcess<VectorF3> = Default::default();
        let kgm: KProcess<VectorF2> = Default::default();
        let va = VectorF3::from(config.accel.0.vars);
        let vg = VectorF3::from(config.gyro.0.vars);
        let vm = VectorF3::from(config.magn.0.vars);
        Ok(Self {
            sampler,
            dcm,
            kga,
            kgm,
            va,
            vg,
            vm,
        })
    }
    pub fn with_args(devsamplers: Vec<DevSampler>) -> Result<()> {
        let running = Arc::new(AtomicBool::new(true));
        let r = running.clone();

        ctrlc::set_handler(move || {
            r.store(false, Ordering::SeqCst);
        })
        .expect("Error setting Ctrl-C handler");

        let matches = App::new(crate_name!())
            .version(crate_version!())
            .author(crate_authors!("\n"))
            .about(crate_description!())
            .arg(
                Arg::with_name("config")
                    .long("config")
                    .help("Set the configuration file to use")
                    .default_value("./config.toml")
                    .value_name("FILE"),
            )
            .arg(
                Arg::with_name("dump_accel")
                    .long("dump_accel")
                    .help("Dump accelerometer data"),
            )
            .arg(
                Arg::with_name("dump_gyro")
                    .long("dump_gyro")
                    .help("Dump gyroscope data"),
            )
            .arg(
                Arg::with_name("dump_magn")
                    .long("dump_magn")
                    .help("Dump magnetometer data"),
            )
            .arg(Arg::with_name("dump_dcm").long("dump_dcm").help("Dump dcm"))
            .arg(
                Arg::with_name("dump_rpy")
                    .long("dump_rpy")
                    .help("Dump roll pitch yaw"),
            )
            .get_matches();

        let mut fusion = Self::new(devsamplers, matches.value_of("config").unwrap())?;

        if matches.is_present("dump_accel") {
            return fusion.dumpaccel(running);
        } else if matches.is_present("dump_gyro") {
            return fusion.dumpgyro(running);
        } else if matches.is_present("dump_magn") {
            return fusion.dumpmagn(running);
        } else if matches.is_present("dump_dcm") {
            return fusion.mainloop(running, Self::forward_dump_dcm);
        } else if matches.is_present("dump_rpy") {
            return fusion.mainloop(running, Self::forward_dump_rpy);
        } else {
            return fusion.mainloop(running, Self::forward_orientation);
        }
    }
    fn dcm_from_roll_pitch_yaw(mva: usize, roll: Float, pitch: Float, yaw: Float) -> MatrixF3 {
        let (cosr, sinr, cosp, sinp, cosy, siny) = (
            roll.cos(),
            roll.sin(),
            pitch.cos(),
            pitch.sin(),
            yaw.cos(),
            yaw.sin(),
        );
        let mut dcm = MatrixF3::zeros();
        let mut ind = Self::mva_to_ind(mva, 0);
        dcm[(0, ind)] = cosp * cosy;
        dcm[(1, ind)] = cosp * siny;
        dcm[(2, ind)] = -sinp;
        ind = Self::mva_to_ind(mva, 1);
        dcm[(0, ind)] = sinr * sinp * cosy - cosr * siny;
        dcm[(1, ind)] = sinr * sinp * siny + cosr * cosy;
        dcm[(2, ind)] = sinr * cosp;
        ind = Self::mva_to_ind(mva, 2);
        dcm[(0, ind)] = cosr * sinp * cosy + sinr * siny;
        dcm[(1, ind)] = cosr * sinp * siny - sinr * cosy;
        dcm[(2, ind)] = cosr * cosp;
        dcm
    }
    /// Index of the most vertical axis of the DCM,
    /// ie. the index of the column in dcm with the largest
    /// absolute z
    fn most_vertical_axis(&self) -> usize {
        self.dcm.row(2).transpose().iamax()
    }
    /// Indirection: what is the DCM's column of vector `ind` when
    /// the most vertical axis is `mva`
    fn mva_to_ind(mva: usize, ind: usize) -> usize {
        (mva + ind + 1) % 3
    }
    fn pitch(&self, mva: usize) -> Float {
        let (indx, indy, indz) = (
            Self::mva_to_ind(mva, 0),
            Self::mva_to_ind(mva, 1),
            Self::mva_to_ind(mva, 2),
        );
        (-self.dcm[(2, indx)])
            .atan2((self.dcm[(2, indy)].powi(2) + self.dcm[(2, indz)].powi(2)).sqrt())
    }
    fn roll(&self, mva: usize) -> Float {
        let (indy, indz) = (Self::mva_to_ind(mva, 1), Self::mva_to_ind(mva, 2));
        self.dcm[(2, indy)].atan2(self.dcm[(2, indz)])
    }
    fn yaw(&self, mva: usize) -> Float {
        let indx = Self::mva_to_ind(mva, 0);
        self.dcm[(1, indx)].atan2(self.dcm[(0, indx)])
    }
    fn orthonormalize_correct(&mut self, mva: usize) {
        self.dcm =
            Self::dcm_from_roll_pitch_yaw(mva, self.roll(mva), self.pitch(mva), self.yaw(mva));
    }
    /// Propagation of the accelerometer input.
    /// This is for the correct phase of the process kga.
    fn accel_input(&mut self, mut a: VectorF3) {
        a.normalize_mut();
        self.kga.compute_gain(&self.va);
        self.kga.x = self.dcm.row(2).transpose();
        self.kga.correct_state(&a);
        self.dcm.set_row(2,&self.kga.x.transpose());
        self.orthonormalize_correct(self.most_vertical_axis());
        self.kga.correct_error();
    }
    /// Propagation of the gyroscope input.
    /// This is for the predict phase of both processes kga
    /// and kgm
    fn gyro_input(&mut self, g: VectorF3, dt: Float) {
        {
            // kga predict phase
            self.kga.x = self.dcm.row(2).transpose();
            let dcm3xdt = self.kga.x * dt;
            let b = MatrixF3::new(
                0.0,
                -dcm3xdt[2],
                dcm3xdt[1],
                dcm3xdt[2],
                0.0,
                -dcm3xdt[0],
                -dcm3xdt[1],
                dcm3xdt[0],
                0.0,
            );
            self.kga.predict_state(&b, &g);

            // compute the process noise covariance
            // It's diagonal, b is skew-symetric with diag == 0
            // So we can speed up computations:
            // pow2(b[i,j) == pow2(b[j,i]), we re-use dcm3xdt
            let p2b = dcm3xdt.map(|e| e.powi(2));
            let q = VectorF3::new(
                p2b[2] * self.vg[1] + p2b[1] * self.vg[2],
                p2b[2] * self.vg[0] + p2b[0] * self.vg[2],
                p2b[1] * self.vg[0] + p2b[0] * self.vg[1],
            );
            self.kga.predict_error(&q);
        }
        let mva = self.most_vertical_axis();
        let indx = Self::mva_to_ind(mva, 0);
        {
            //kgm predict phase
            self.kgm.x[0] = self.dcm[(0, indx)];
            self.kgm.x[1] = self.dcm[(1, indx)];
            let (b, sub_g, sub_v) = match mva {
                0 => (
                    MatrixF2::new(
                        self.dcm[(0, 2)],
                        -self.dcm[(0, 0)],
                        self.dcm[(1, 2)],
                        -self.dcm[(1, 0)],
                    ),
                    VectorF2::new(g[0], g[2]),
                    VectorF2::new(self.vg[0], self.vg[2]),
                ),
                1 => (
                    MatrixF2::new(
                        -self.dcm[(0, 1)],
                        self.dcm[(0, 0)],
                        -self.dcm[(1, 1)],
                        self.dcm[(1, 0)],
                    ),
                    VectorF2::new(g[0], g[1]),
                    VectorF2::new(self.vg[0], self.vg[1]),
                ),
                2 => (
                    MatrixF2::new(
                        -self.dcm[(0, 2)],
                        self.dcm[(0, 1)],
                        -self.dcm[(1, 2)],
                        self.dcm[(1, 1)],
                    ),
                    VectorF2::new(g[1], g[2]),
                    VectorF2::new(self.vg[1], self.vg[2]),
                ),
                _ => unreachable!(),
            };
            let b = b * dt;
            self.kgm.predict_state(&b, &sub_g);
            let q = VectorF2::from_iterator(
                b.transpose()
                    .column_iter()
                    .map(|c| c.zip_fold(&sub_v, 0.0, |acc, b, v| acc + (b.powi(2) * v))),
            );
            self.kgm.predict_error(&q);
        }

        // now we merge results into the DCM
        self.dcm.set_row(2, &self.kga.x.transpose());
        self.dcm[(0, indx)] = self.kgm.x[0];
        self.dcm[(1, indx)] = self.kgm.x[1];

        // and we orthonormalize
        let errd2 = (self.dcm.row(1).dot(&self.dcm.row(2))) / 2.0;
        let y_ortho = self.dcm.row(1) - (self.dcm.row(2) * errd2);
        let z_ortho = self.dcm.row(2) - (self.dcm.row(1) * errd2);
        let x_ortho = y_ortho.cross(&z_ortho);
        let x_ortho = x_ortho.normalize();
        let y_ortho = y_ortho.normalize();
        let z_ortho = z_ortho.normalize();
        self.dcm = MatrixF3::from_rows(&[x_ortho, y_ortho, z_ortho]);
    }
    /// Propagation of the magnetometer input.
    /// This is for the correct phase of the process 1.
    fn magn_input(&mut self, mut m: VectorF3) {
        m.normalize_mut();
        let mva = self.most_vertical_axis();
        let indx = Self::mva_to_ind(mva, 0);
        self.kgm.x[0] = self.dcm[(0, indx)];
        self.kgm.x[1] = self.dcm[(1, indx)];
        // Projection to the horizontal plane of magnetometer readings
        // Also computes covariance using propagation covariance law in `r`
        // using covariances of magnetometers (`self.vm`)
        let (roll, pitch) = (self.roll(mva), self.pitch(mva));
        let (cosr, sinr, cosp, sinp) = (roll.cos(), roll.sin(), pitch.cos(), pitch.sin());
        let (indy, indz) = (Self::mva_to_ind(mva, 1), Self::mva_to_ind(mva, 2));
        let hm = VectorF2::new(
            m[indx] * cosp + m[indy] * sinr * sinp + m[indz] * cosr * sinp,
            -(m[indy] * cosr - m[indz] * sinr),
        );
        let r = VectorF2::new(
            cosp.powi(2) * self.vm[indx]
                + (sinr * sinp).powi(2) * self.vm[indy]
                + (cosr * sinp).powi(2) * self.vm[indz],
            cosr.powi(2) * self.vm[indy] + sinr.powi(2) * self.vm[indz],
        );
        self.kgm.compute_gain(&r);
        self.kgm.correct_state(&hm);
        // integrate into dcm
        self.dcm[(0, indx)] = self.kgm.x[0];
        self.dcm[(1, indx)] = self.kgm.x[1];
        self.orthonormalize_correct(mva);
        self.kgm.correct_error();
    }

    fn dump<F>(&mut self, running: Arc<AtomicBool>, f: F) -> Result<()>
    where
        F: Fn(Sample) -> Option<Sample>,
    {
        while running.load(Ordering::SeqCst) {
            let sample = self.sampler.next().unwrap()?;
            if let Some(sample) = f(sample) {
                match sample {
                    Sample::Accel(v) | Sample::Magn(v) =>
                        println!("{:+.5} {:+.5} {:+.5}", v[0], v[1], v[2]),
                    Sample::Gyro(v,t) =>
                        println!("{:+.5} {:+.5} {:+.5} {:?}", v[0], v[1], v[2], t),
                }
            }
        }
        Ok(())
    }

    fn dumpaccel(&mut self, running: Arc<AtomicBool>) -> Result<()> {
        self.dump(running, |sample| match sample {
            Sample::Accel(_) => Some(sample),
            _ => None,
        })
    }

    fn dumpgyro(&mut self, running: Arc<AtomicBool>) -> Result<()> {
        self.dump(running, |sample| match sample {
            Sample::Gyro(_, _) => Some(sample),
            _ => None,
        })
    }

    fn dumpmagn(&mut self, running: Arc<AtomicBool>) -> Result<()> {
        self.dump(running, |sample| match sample {
            Sample::Magn(_) => Some(sample),
            _ => None,
        })
    }

    fn forward_orientation(fusion: &Self) -> Result<()> {
        let mut stdout = stdout();
        let m = fusion.dcm.transpose().map(|x| x as f32);
        let s =
            unsafe { from_raw_parts((&m as *const Matrixf3) as *const u8, size_of::<Matrixf3>()) };
        stdout.write_all(&s)?;
        stdout.flush()?;
        Ok(())
    }

    fn forward_dump_dcm(fusion: &Self) -> Result<()> {
        println!("{}", fusion.dcm);
        Ok(())
    }

    fn forward_dump_rpy(fusion: &Self) -> Result<()> {
        let mva = fusion.most_vertical_axis();
        println!(
            "{:+.5} {:+.5} {:+.5}",
            fusion.roll(mva),
            fusion.pitch(mva),
            fusion.yaw(mva)
        );
        Ok(())
    }

    fn mainloop<F>(&mut self, running: Arc<AtomicBool>, forward: F) -> Result<()>
    where
        F: Fn(&Self) -> Result<()>,
    {
        let first_gyro_sample = self.sampler.find(|x| {
            if x.is_err() {
                return true;
            } else if let Ok(Sample::Gyro(_, _)) = x {
                return true;
            } else {
                return false;
            }
        });
        let mut last_t = match first_gyro_sample {
            None => return Err(Error::new(ErrorKind::Other, "Can't get gyroscope sample")),
            Some(Err(e)) => return Err(e),
            Some(Ok(Sample::Gyro(_, t))) => t,
            _ => unreachable!(),
        };
        while running.load(Ordering::SeqCst) {
            let sample = self.sampler.next().unwrap()?;
            match sample {
                Sample::Accel(v) => self.accel_input(v),
                Sample::Gyro(v, t) => {
                    let dt = t - last_t;
                    last_t = t;
                    self.gyro_input(v, as_secs_f(dt))
                }
                Sample::Magn(v) => {
                    self.magn_input(v);
                    // We only forward on magnetometer input, that's enough
                    forward(&self)?;
                }
            }
        }
        Ok(())
    }
}
