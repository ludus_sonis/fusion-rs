/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/fusion.
 *
 * Fusion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fusion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Fusion.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::sample::Sample;

use std::fs::File;
use std::io::Result;

/// A particular sampler as [crate::samplers::lis3mdl::Lis3mdl] must implement this trait
pub trait DevSamplerTrait {
    fn get_sample(&mut self, rdr: &mut File) -> Result<Option<Sample>>;
}

/// This device is connected to one iio device. This iio device might be multiple sensors.
pub struct DevSampler {
    pub(crate) rdr: File,
    pub(crate) dst: Box<dyn DevSamplerTrait>,
}

impl DevSampler {
    fn new(file: &str, dst: Box<dyn DevSamplerTrait>) -> Result<Self> {
        let rdr = File::open(file)?;
        Ok(Self { rdr, dst })
    }
    pub fn from_dst(dst: impl DevSamplerTrait + 'static, file: &str) -> Result<Self> {
        Self::new(file, Box::new(dst))
    }
}
