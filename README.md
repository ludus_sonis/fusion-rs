mostly based on doi:10.3390/s18082616

Some notations from "An Introduction to the Kalman Filter",
Greg Welch and Gary Bishop
Some from "A Fusion Method for Combining Low-Cost IMU/Magnetometer
Outputs for Use in Applications on Mobile Devices"
doi:10.3390/s18082616

In "A Fusion..." R is the rotation matrix or Direct Cosine Matrix,
the state. But R is also the letter used for the measurement noise
covariance matrix. Here the Direct Cosine Matrix is named DCM.
